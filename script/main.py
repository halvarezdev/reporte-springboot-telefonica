import requests
import json

url = 'http://localhost:9000/api/report'
urlmail = 'http://localhost:9000/api/email'

headers = {
    'User-Agent': 'PythonAgent',
    'Agent-Version': '1.0.0',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json',
    'Accept': '*/*'
}

map = json.dumps([
    {
        "id": 10,
        "responseCode": 0
    },
    {
        "id": 11,
        "responseCode": 0
    },
    {
        "id": 12,
        "responseCode": 0
    },
    {
        "id": 13,
        "responseCode": 0
    }
])


response_report = requests.post(url, data=map, headers=headers)
resp = json.JSONDecoder().decode(response_report.text)

map_email = json.dumps({
    "to": ["jorge.cerna@telefonica.com","jordi.ovalle@telefonica.com"],
    "cc": ["jjmeza@soaint.com","eremigio@soaint.com","jflores@soaint.com", "jecastillo@soaint.com","atippe@soaint.com","halvarez@soaint.com"],
    "subject": "[◉]ESTADO DEL MOTOR DE OFERTAS",
    "message": resp
})

response_email = requests.post(url=urlmail, data=map_email, headers=headers)




