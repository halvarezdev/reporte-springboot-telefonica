package pe.telefonica.reports.service;


import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import pe.telefonica.reports.util.Util;

@Service
public class ConsultaService {

    RestTemplate restTemplate = new RestTemplate();
    Util utilObj = new Util();

//    @Async
//    public Response getServicio(pe.telefonica.reports.model.Service serv) {
//        org.apache.catalina.connector.Response re = new org.apache.catalina.connector.Response();
//
//        try {
//            WebTarget wt = ClientBuilder.newClient().target(serv.getUrl());
//            return wt.path("?" + serv.getPayload()).request().get();
//        }catch (Exception e) {
//            System.out.println("Error :: consulta :: " + e.getMessage());
//            return Response.serverError().entity(e.getMessage()).build();
//        }
//
//    }

    @Async
    public ResponseEntity<String> getServicio(String url) {
        HttpHeaders headers = (HttpHeaders) utilObj.getHttpHeaders();
        try {
            return restTemplate.exchange("http://"+url, HttpMethod.GET, new HttpEntity<>(headers),String.class);
        }catch (ResourceAccessException acc){
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }

    }

}
