package pe.telefonica.reports.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;

import java.util.*;

import pe.telefonica.reports.dto.ServiceDTO;
import pe.telefonica.reports.repository.IServiceRepository;
import pe.telefonica.reports.util.Util;

@Service
public class ReportService {
    @Autowired
    IServiceRepository serviceRepository;

    @Autowired
    ConsultaService consultaClient;

    Util util = new Util();

    public List<pe.telefonica.reports.model.Service> getAllServices() {
        List<pe.telefonica.reports.model.Service> listServ = new ArrayList<pe.telefonica.reports.model.Service>();

        serviceRepository.findAll().forEach(listServ::add);

        return listServ;
    }
/*
    public List<ServiceDTO> consultarListServices(List<ServiceDTO> serviceDTOList) {

        List<pe.telefonica.reports.model.Service> listServ = serviceRepository.findAll();
        List<ServiceDTO> resultList = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        ResponseEntity<String> exp = new ResponseEntity<>(HttpStatus.NO_CONTENT);

        if (!listServ.isEmpty()) {

            for (ServiceDTO sDTO : serviceDTOList) {

                for (pe.telefonica.reports.model.Service serv : listServ) {

                    if (sDTO.getId() == serv.getId()) {

                        try {
                            response = consultaClient
                                    .getServicio(serv.getUrl() + "?" + serv.getPayload());

                        } catch (Exception e) {
                            exp = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);

                        }

                        if (exp.getStatusCode().value() != 204) {
                            sDTO.setId(serv.getId());
                            sDTO.setNameService(serv.getNombre());
                            sDTO.setCountObjectsBody(gson.toJson(exp.getBody()).length());
                            sDTO.setMessage("El servicio se encuentra: " + exp.getStatusCode().getReasonPhrase()
                                    + " reason: " + exp.getBody());
                            sDTO.setResponseCode(exp.getStatusCode().value());
                        } else {
                            sDTO.setId(serv.getId());
                            sDTO.setNameService(serv.getNombre());
                            sDTO.setCountObjectsBody(gson.toJson(response.getBody()).length());
                            sDTO.setMessage("El servicio se encuentra: " + response.getStatusCode().getReasonPhrase());
                            sDTO.setResponseCode(response.getStatusCode().value());
                        }

                    }

                }
                resultList.add(sDTO);
            }
        }

        return resultList;
    }*/
    
    public List<ServiceDTO> consultarListServices(List<ServiceDTO> serviceDTOList) {

        List<pe.telefonica.reports.model.Service> listServ = serviceRepository.findAll();
        List<ServiceDTO> resultList = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();

        if (!listServ.isEmpty()) {

            for (ServiceDTO sDTO : serviceDTOList) {

                for (pe.telefonica.reports.model.Service serv : listServ) {

                    if (sDTO.getId() == serv.getId()) {
                        ResponseEntity<String> response = consultaClient
                                .getServicio(serv.getUrl() + "?" + serv.getPayload());

                        if (response.getStatusCode() != HttpStatus.OK) {
                            sDTO.setId(serv.getId());
                            sDTO.setNameService(serv.getNombre());
                            sDTO.setCountObjectsBody(gson.toJson(response.getBody()).length());
                            sDTO.setMessage("El servicio se encuentra: " +
                                    response.getStatusCode().getReasonPhrase()
                                    + response.getBody());
                            sDTO.setResponseCode(response.getStatusCode().value());
                        } else {
                            sDTO.setId(serv.getId());
                            sDTO.setNameService(serv.getNombre());
                            sDTO.setCountObjectsBody(gson.toJson(response.getBody()).length());
                            sDTO.setMessage("El servicio se encuentra: " +
                                    response.getStatusCode().getReasonPhrase());
                            sDTO.setResponseCode(response.getStatusCode().value());
                        }

                    }

                }
                resultList.add(sDTO);
            }
        }

        return resultList;
    }

    public List<ServiceDTO> consultarListServicesByProyectoId(String proyectoId) {

        List<pe.telefonica.reports.model.Service> listServ = serviceRepository.findServiceByIdProyecto(proyectoId);
        List<ServiceDTO> resultList = new ArrayList<>();

        if (!listServ.isEmpty()) {
            for (pe.telefonica.reports.model.Service serv : listServ) {
                ResponseEntity<String> response = consultaClient.getServicio(serv.getUrl() + "?" + serv.getPayload());
                ServiceDTO sDTO = new ServiceDTO();

                GsonBuilder builder = new GsonBuilder();
                builder.setPrettyPrinting();
                Gson gson = builder.create();

                sDTO.setId(serv.getId());
                sDTO.setNameService(serv.getNombre());
                sDTO.setResponseCode(response.getStatusCode().value());
                sDTO.setCountObjectsBody(gson.toJson(response.getBody()).length());
                sDTO.setMessage("El servicio se encuentra: " + response.getStatusCode().getReasonPhrase());

                resultList.add(sDTO);
            }

        }

        return resultList;
    }

}