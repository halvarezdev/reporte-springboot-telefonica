package pe.telefonica.reports.service;

import pe.telefonica.reports.entity.Users;
import pe.telefonica.reports.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UsersRepository repository;


    public List<Users> getUsers( ) {
        return repository.findAll();
    }

    public Users saveUser(Users users) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodePassword = passwordEncoder.encode(users.getPassword());
        users.setPassword(encodePassword);
        return repository.save(users);
    }

    public String deleteUser(int id) {
        repository.deleteById(id);
        return "Usuario Eliminado: " + id;
    }

    public Users updateUser(Users users) {
    	
    	Users existingUser = repository.findById(users.getId()).orElse(null);
        existingUser.setName(users.getName());
        existingUser.setEmail(users.getEmail());
        existingUser.setAddress(users.getAddress());
        existingUser.setPassword(users.getPassword());
        return repository.save(existingUser);
    }

    public Users getUserById(int id) {
        return repository.findById(id).orElse(null);
    }

    public Users getProductByName(String name) {
        return repository.findByName(name);
    }

}
