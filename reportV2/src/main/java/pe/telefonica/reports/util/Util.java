package pe.telefonica.reports.util;

import org.springframework.http.HttpHeaders;
import pe.telefonica.reports.dto.ServiceDTO;

import java.util.HashMap;
import java.util.List;

public class Util {

    public HttpHeaders getHttpHeaders(){
        HashMap<String,String> mapHeaders = new HashMap<>();
        mapHeaders.put("Postman-Token","<calculated when request is sent>");
        mapHeaders.put("Host","<calculated when request is sent>");
        //mapHeaders.put("User-Agent","PostmanRuntime/7.28.4");
        mapHeaders.put("Accept","*/*");
        mapHeaders.put("Accept-Encoding","gzip, deflate, br");
        mapHeaders.put("Connection","keep-alive");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(mapHeaders);
        return httpHeaders;
    }

    public Integer getPositionServiceById(List<ServiceDTO> listServices, Long id){
        Integer position=0;

        for (ServiceDTO s:listServices){
            if (s.getId()==id){
                break;
            }
            position++;
        }

        return position;
    }

}
