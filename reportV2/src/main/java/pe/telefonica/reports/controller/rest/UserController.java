package pe.telefonica.reports.controller.rest;

import java.util.List;

import pe.telefonica.reports.entity.Users;
import pe.telefonica.reports.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

  @Autowired
  UserService service;

  @CrossOrigin(origins = "*", methods = { RequestMethod.POST })
  @PostMapping("/addUser")
  public Users addUser(@RequestBody Users users) {
    return service.saveUser(users);
  }

  @CrossOrigin(origins = "*", methods = { RequestMethod.GET })
  @GetMapping("/users")
  public List<Users> findAllUsers() {
    return service.getUsers();
  }

  @CrossOrigin(origins = "*", methods = { RequestMethod.DELETE })
  @DeleteMapping("/delete/{id}")
  public String deleteUser(@PathVariable int id) {
    return service.deleteUser(id);
  }

  @CrossOrigin(origins = "*", methods = { RequestMethod.PUT })
  @PutMapping("/update")
  public Users updateUser(@RequestBody Users user) {
    return service.updateUser(user);
  }

}
