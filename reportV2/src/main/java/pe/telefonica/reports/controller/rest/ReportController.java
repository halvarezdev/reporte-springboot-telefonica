package pe.telefonica.reports.controller.rest;

import java.util.ArrayList;
import java.util.List;

import pe.telefonica.reports.dto.ServiceDTO;
import pe.telefonica.reports.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.telefonica.reports.service.ReportService;

@RestController
@RequestMapping("/api")
public class ReportController {

  @Autowired
  ReportService reportClient;

  @CrossOrigin(origins = "*", methods = { RequestMethod.POST })

  @PostMapping("/report")
  public ResponseEntity<List<ServiceDTO>> getReportOfferingServices(@RequestBody List<ServiceDTO> listServicesDTO) {

    try {
      List<ServiceDTO> result = new ArrayList<ServiceDTO>();

      reportClient.consultarListServices(listServicesDTO).forEach(result::add);

      if (result.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*
   * @CrossOrigin(origins = "*", methods = { RequestMethod.POST })
   * 
   * @PostMapping("/report")
   * public List<ServiceDTO> getReportOfferingServices(@RequestBody
   * List<ServiceDTO> listServicesDTO) {
   * 
   * return reportClient.consultarListServices(listServicesDTO);
   * 
   * }
   */

  @CrossOrigin(origins = "*", methods = { RequestMethod.GET })
  @GetMapping("/services")
  public ResponseEntity<List<Service>> getAllTutorials() {
    try {
      List<Service> listServ = new ArrayList<Service>();

      reportClient.getAllServices().forEach(listServ::add);

      if (listServ.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(listServ, HttpStatus.OK);
    } catch (Exception e) {

      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin(origins = "*", methods = { RequestMethod.GET })
  @GetMapping("/report/{id}")
  public ResponseEntity<List<ServiceDTO>> getReportOfferingServicesById(@PathVariable String id) {
    try {
      List<ServiceDTO> result = new ArrayList<ServiceDTO>();

      reportClient.consultarListServicesByProyectoId(id).forEach(result::add);

      if (result.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);

    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

}
