package pe.telefonica.reports.controller.rest;

import pe.telefonica.reports.repository.IEmailSenderServiceRepository;
import pe.telefonica.reports.model.EmailMessage;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/api")

public class EmailController {

    private final IEmailSenderServiceRepository emailSenderService;

    public EmailController(IEmailSenderServiceRepository emailSenderService) {

        this.emailSenderService = emailSenderService;
    }

    @CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.OPTIONS })
    @PostMapping("/email")

    public ResponseEntity sendEmail(@RequestBody EmailMessage emailMessage) {

        this.emailSenderService.sendEmail(emailMessage.getTo(), emailMessage.getCc(), emailMessage.getSubject(), emailMessage.getMessage());

        return ResponseEntity.ok("Success");
    }
}
