package pe.telefonica.reports.model;
import java.util.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailMessage {

    private String[] to;
    private String[] cc;
    private String subject;
    private List<Message> message;
    

 
}
