package pe.telefonica.reports.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Message {

    private int id;
    private String nameService;
    private int responseCode;
    private int countObjectsBody;
    private String message;

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("Id: " + id + "\n");
        sb.append("Nombre del servicio: " + nameService+ "\n");
        sb.append("Response code: " + responseCode + "\n");
        sb.append("Cantidad Objectos: " + countObjectsBody + "\n");
        sb.append("Estado del Servicio: " + message + "\n");
        sb.append("▬▬▬▬▬▬"+ "\n");

        return sb.toString();
    }

}
