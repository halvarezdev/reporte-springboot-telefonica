package pe.telefonica.reports.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Service {

  private long id;
  private String url;
  private String payload;
  private String nombre;
  private Integer proyecto_id;
  private String state;

}
