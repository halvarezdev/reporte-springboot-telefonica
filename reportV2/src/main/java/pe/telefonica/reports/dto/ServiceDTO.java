package pe.telefonica.reports.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor

public class ServiceDTO {

    private Long id;
    private String nameService;
    private Integer responseCode;
    private Integer countObjectsBody;
    private String message;

   
}
