package pe.telefonica.reports.repository.Impl;
import pe.telefonica.reports.model.Message;
import  pe.telefonica.reports.repository.IEmailSenderServiceRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderServiceImpl implements IEmailSenderServiceRepository {

    @Autowired
    private final JavaMailSender mailSender;

    

    public EmailSenderServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmail(String[] to, String[] cc, String subject, List<Message> messages) {

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("reportes.soaint@gmail.com");
        simpleMailMessage.setTo(to);
        simpleMailMessage.setCc(cc);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(messages.toString());
        this.mailSender.send(simpleMailMessage);
    }
}
    
