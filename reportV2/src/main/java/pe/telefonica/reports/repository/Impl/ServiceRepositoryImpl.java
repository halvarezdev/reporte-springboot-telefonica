package pe.telefonica.reports.repository.Impl;

import java.util.List;

import pe.telefonica.reports.model.Service;
import pe.telefonica.reports.repository.IServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceRepositoryImpl implements IServiceRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Override
  public List<Service> findAll() {
    return jdbcTemplate.query("SELECT * from reportes.service where state ='on' order by id desc", BeanPropertyRowMapper.newInstance(Service.class));
  }

  public List<Service> findServiceByIdProyecto(String idProyecto) {
    return jdbcTemplate.query("SELECT * from reportes.service where state='on' and proyecto_id ="+ idProyecto, BeanPropertyRowMapper.newInstance(Service.class));
  }

  public List<Service> findAllProjects() {
    return jdbcTemplate.query("SELECT * from reportes.proyecto", BeanPropertyRowMapper.newInstance(Service.class));
  }


  public List<Service> findAllProjectsById(String id) {
    return jdbcTemplate.query("SELECT * from reportes.proyecto where id="+id, BeanPropertyRowMapper.newInstance(Service.class));
  }

}

