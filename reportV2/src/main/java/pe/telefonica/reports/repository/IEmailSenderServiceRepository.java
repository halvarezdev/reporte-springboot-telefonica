package pe.telefonica.reports.repository;

import java.util.List;

import pe.telefonica.reports.model.Message;

public interface IEmailSenderServiceRepository {
  
void sendEmail(String[] to, String[] cc, String subject, List<Message> messages);

 
    
}
