package pe.telefonica.reports.repository;

import java.util.List;

import pe.telefonica.reports.model.Service;

public interface IServiceRepository {

  List<Service> findAll();

  List<Service> findServiceByIdProyecto(String idProyecto);

  List<Service> findAllProjects();

  List<Service> findAllProjectsById(String id);

}
