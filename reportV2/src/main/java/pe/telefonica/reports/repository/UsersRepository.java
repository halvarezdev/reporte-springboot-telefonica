package pe.telefonica.reports.repository;
import pe.telefonica.reports.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Integer> {
    Users findByName(String name);
}
           